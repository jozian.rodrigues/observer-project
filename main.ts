import * as readlineSync from 'readline-sync';

class Observador {
    atualizar(): void {
        console.log("Evento acionado");
    }
}

class Sujeito {
    private observadores: Observador[] = [];

    adicionarObservador(observador: Observador): void {
        this.observadores.push(observador);
    }

    notificarObservadores(): void {
        this.observadores.forEach(observador => observador.atualizar());
    }
}

class TextEditor extends Sujeito {
    private linhas: string[] = [];

    insertLine(lineNumber: number, text: string): void {
        this.linhas.splice(lineNumber - 1, 0, text);
        this.notificarObservadores();
    }

    salvarArquivo(): void {
        console.log("Conteúdo do arquivo:");
        this.linhas.forEach(line => console.log(line));
    }
}

class ConsoleObserver implements Observador {
    atualizar(): void {
        console.log("O evento foi acionado");
    }
}

function main(): void {
    const textEditor = new TextEditor();
    textEditor.adicionarObservador(new ConsoleObserver());

    let lineNumber = 1;
    console.log(`Inserir linhas no editor (digite 'EOF' para encerrar):`);

    let text = readlineSync.question(`Inserir linha ${lineNumber}: `);

    while (text.toLowerCase() !== 'eof') {
        textEditor.insertLine(lineNumber, text);
        lineNumber++;
        text = readlineSync.question(`Inserir linha ${lineNumber} (ou 'EOF' para encerrar): `);
    }

    textEditor.salvarArquivo();
}

main();
